-------------------------------------------------------------------------------
-- Marc Zuo's xmonad configuration file ---------------------------------------
-------------------------------------------------------------------------------

import           XMonad
import           XMonad.Config.Azerty (azertyKeys)
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import           XMonad.Layout
import           XMonad.Layout.NoBorders (smartBorders, hasBorder)
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.Grid
import           XMonad.Layout.Reflect
import qualified XMonad.Layout.TwoPane as TP
import qualified XMonad.Hooks.EwmhDesktops as ED
import           XMonad.Util.Run (spawnPipe)
import           XMonad.Util.EZConfig (additionalKeys)
import           Control.Exception 

-- Local libraries in directory lib/
import           PowerlineLogHook
import           ClickableWorkspaces
import           FullscreenFix

-- Here you can specify actions for individual application's windows.
myManageHook = composeAll

    -- Dialog windows to float
    [ className =? "Gxmessage" --> doFloat
    , (className =? "Firefox" <&&> resource =? "Dialog") --> doFloat 
    , className =? "Pinentry" --> doFloat
    , className =? "Gmrun" --> doFloat

    -- For some reason we can only catch gimp dialogs by roleName
    , roleName =? "gimp-message-dialog" --> doFloat
    , roleName =? "gimp-toolbox-color-dialog" --> doFloat
    , roleName =? "gimp-dock" --> doFloat
    , roleName =? "gimp-startup" --> hasBorder False

    -- Arrange windows into workspaces
    , className =? "Steam" --> doShift (myWorkspaces !! 4)

    -- catch all full screen handler
    , isFullscreen --> doFullFloat

    -- Ignore wine-wechat windows
    , appName =? "wechat.exe" --> doIgnore

    -- Force Tor Browser to float to prevent viewport size-based fingerprinting
    -- Default letterboxing in Tor Browser mitigates this somewhat
    --, className =? "Tor Browser" --> doFloat

    ] where roleName = stringProperty "WM_WINDOW_ROLE"

-- A list of additional key bindings
myAdditionalKeys = 
    [ ((modm              , xK_f), spawn "firefox")
    , ((modm .|. shiftMask, xK_f), spawn "freetube")
    , ((modm              , xK_p), spawn "dmenu_extended_run 2>&1 >/dev/null")
    , ((modm              , xK_e), spawn "nemo")
    , ((modm .|. shiftMask, xK_m), spawn "st -e neomutt")
    , ((modm .|. shiftMask, xK_z), spawn "xautolock -locknow")
    , ((modm              , xK_z), spawn "xpoweroff")
    , ((mod4Mask          , xK_c), spawn "st -e chatall")
    , ((modm .|. shiftMask, xK_t), spawn "focuswriter")
    , ((modm .|. shiftMask, xK_s), spawn "$HOME/.xmonad/bin/xscreensaver-helper -t")
    , ((modm .|. shiftMask, xK_r), spawn "$HOME/.xmonad/bin/redshift-helper -t")
    , ((modm .|. shiftMask, xK_n), spawn "st -e newsboat")
    , ((modm              , xK_Print), spawn "maim -u -i $(xdotool selectwindow) ~/Images/window_$(date +%Y-%m-%d_%H-%M-%S).png") 
    , ((modm .|. shiftMask, xK_Print), spawn "maim -u -s ~/Images/window_$(date +%Y-%m-%d_%H-%M-%S).png") 
    , ((modm              , xK_Return), sendMessage $ Toggle NBFULL)
    , ((modm              , xK_Page_Up), spawn "playerctl -p ncspot previous")
    , ((modm              , xK_Page_Down), spawn "playerctl -p ncspot next")
    , ((modm              , xK_End), spawn "playerctl -a play-pause")
    , ((modm .|. shiftMask, xK_End), spawn "playerctl -a stop")
    , ((modm              , xK_space), sendMessage $ Toggle MIRROR)
    , ((modm .|. shiftMask, xK_space), sendMessage $ Toggle REFLECTX)
    , ((modm              , xK_Up), sendMessage $ FirstLayout)
    , ((modm              , xK_Down), sendMessage $ NextLayout)
    ]

-- Set the modifier key (Alt_L)
modm = mod1Mask

-- Set layout hook
myLayoutHook = avoidStruts $ smartBorders
    $ mkToggle (REFLECTX ?? MIRROR ?? NBFULL ?? EOT)
    $ myTall ||| myTwoPane ||| Grid
    where
        myTall = Tall nmaster delta ratio
        myTwoPane = TP.TwoPane delta ratio
        nmaster = 1
        ratio = 1/2
        delta = 3/100

-- Applications to start automatically
-- DO NOT put autostart per X session applications here! The applications
-- listed here will be started again every time xmonad exits.
myStartupHook = def <+> spawn "$HOME/.xmonad/bin/mopidy-helper"
                    <+> spawn "$HOME/.xmonad/bin/xscreensaver-helper"
                    <+> spawn "$HOME/.xmonad/bin/easyeffects-helper"
                    <+> spawn "$HOME/.xmonad/bin/redshift-helper"
                    <+> spawn "$HOME/.xmonad/bin/date-helper -w"

-- Use clickable workspaces
myWorkspaces = clickableWorkspaces workspaceNames where 
        workspaceNames = map (wrap " " " ") ["α","β","γ","δ","ε","ς","ζ","η","θ","ι"] 

main = do
        -- Launch xmobar
        xmproc <- spawnPipe "xmobar"

        -- Launch xmonad
        xmonad $ docks $ fullscreenFix $ ED.ewmhFullscreen $ ED.ewmh $ def
            { layoutHook = myLayoutHook
            , manageHook = manageDocks <+> myManageHook <+> manageHook def
            --, handleEventHook = ED.fullscreenEventHook <+> handleEventHook def
            , startupHook = myStartupHook

            -- from module lib/ClickableWorkspaces
            , workspaces = myWorkspaces

            -- Set the modifier key (default Alt_L)
            , modMask = modm

            -- Disable focus following mouse
            , focusFollowsMouse = False

            -- Set the focused window border style
            , focusedBorderColor = "#6c71c4"
            , normalBorderColor = "#073642"
            , borderWidth = 3 

            -- Set the default terminal application
            , terminal = "st -e tmux"

            -- Use logging to output to xmobar
            -- from module lib/PowerlineLogHook
            , logHook = powerlineLogHook xmproc

            -- Enable azerty keyboard support
            , keys = azertyKeys <+> keys def

            } `additionalKeys` myAdditionalKeys
