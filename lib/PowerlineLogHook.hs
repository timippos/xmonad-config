module PowerlineLogHook where

import XMonad
import XMonad.Hooks.DynamicLog
import System.IO

-- Log hook to send information to xmobar
powerlineLogHook xmproc =
    let lambdaLogger = io $ return $ Just $
            (wrap "<action=dmenu_extended_run>" "</action>" $
                xmobarColor "#fdf6e3" "#93a1a1" " \62211 ")
            ++ (xmobarColor "#93a1a1" "#002b36" "\57520")
        clickableLayout = wrap "<action=xdotool key alt+Return>" "</action>"
        surroundWithPowerlineArrow fgc bgc bgcbefore bgcafter =
            let powerlineArrowBefore = xmobarColor bgcbefore bgc "\57520"
                powerlineArrowAfter = xmobarColor bgc bgcafter "\57520" in
            wrap powerlineArrowBefore powerlineArrowAfter . xmobarColor fgc bgc 
        in
        dynamicLogWithPP def
            { ppOutput = hPutStrLn xmproc
            , ppTitle = xmobarColor "#93a1a1" "" . (" " ++)
            , ppCurrent = surroundWithPowerlineArrow "#eee8d5" "#657b83" "#002b36" "#002b36"
            , ppLayout = surroundWithPowerlineArrow "#eee8d5" "#657b83" "#002b36" "#002b36" .
                clickableLayout . wrap " " " "
            , ppSep = ""
            , ppExtras = [lambdaLogger]
            , ppOrder = \(workspaces:layout:title:lambda:rest) -> (lambda:workspaces:layout:title:rest)
            , ppTitleSanitize = xmobarRaw
            }

