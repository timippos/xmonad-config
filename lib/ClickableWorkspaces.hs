module ClickableWorkspaces where

import XMonad
import XMonad.Hooks.DynamicLog

-- Set workspaces to enable click-to-switch
-- Adapted from https://arch-ed.dk/xmobar-clickable-workspaces
-- This version requires wmctrl as a dependency
clickableWorkspaces workspaceNames = clickable . (map xmobarEscape) $ workspaceNames
    where
        clickable l = [ "<action=wmctrl -s " ++ show i ++ ">" ++ ws ++ "</action>" |
                        (i,ws) <- zip [0..length l] l
                      ]

        xmobarEscape = concatMap doubleLts
            where
                doubleLts '<' = "<<"
                doubleLts x   = [x]
