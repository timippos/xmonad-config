==Note to self==

To update a subbranch:

    cd xmonad-git
    git branch master
    git pull origin master

Then, examine if new releases have been added, and if necessary

    git branch -f stable <target_commit>

Or else
    
    git brach stable

Finally build & install if needed.
